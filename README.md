# Alliance Auth Community Creations

A directory of community sourced plugins, tools, resources, etc. for Alliance Auth

## Contents

- [Plugins](#plugins)
- [Services](#services)
- [Deployment and Administration](#deployment-and-administration)
- [Development and Infrastructure](#development-and-infrastructure)

## Plugins

- [Basraah / allianceauth-idp](https://gitlab.com/basraah/allianceauth-idp) - SAML 2.0 Identity Provider for Alliance Auth
- [colcrunch / fittings](https://gitlab.com/colcrunch/fittings) - A plugin for managing fittings and doctrines.
- [colcrunch / aa-moonstuff](https://gitlab.com/colcrunch/aa-moonstuff) - A plugin for publishing moon extractions, and keeping track of moon scan data.
- [Erik Kalkoken / aa-discordnotify](https://gitlab.com/ErikKalkoken/aa-discordnotify) - This app automatically forwards Alliance Auth notifications to users on Discord.
- [Erik Kalkoken / aa-freight](https://gitlab.com/ErikKalkoken/aa-freight.git) - A plugin for running a central freight service for an alliance.
- [Erik Kalkoken / aa-killtracker](https://gitlab.com/ErikKalkoken/aa-killtracker) - An app for running killmail trackers with Alliance Auth and Discord
- [Erik Kalkoken / aa-mailrelay](https://gitlab.com/ErikKalkoken/aa-mailrelay) - An app for relaying Eve mails to Discord.
- [Erik Kalkoken / aa-memberaudit](https://gitlab.com/ErikKalkoken/aa-memberaudit) - An app that provides full access to Eve characters and related reports for auditing, vetting and monitoring.
  - [Eclipse Expeditions / aa-memberaudit-securegroups](https://gitlab.com/eclipse-expeditions/aa-memberaudit-securegroups) - Integration with [Secure Groups](https://github.com/Solar-Helix-Independent-Transport/allianceauth-secure-groups) for automatic group management based on activity, character age, assets, compliance, SP, or skill sets.
- [Erik Kalkoken / aa-moonmining](https://gitlab.com/ErikKalkoken/aa-moonmining) - App for tracking moon extractions and scouting new moons.
- [Erik Kalkoken / aa-standingssync](https://gitlab.com/ErikKalkoken/aa-standingssync.git) - Enables non-alliance characters like scout alts to have the same standings view in game as their alliance main
- [Erik Kalkoken / standingsrequests](https://gitlab.com/ErikKalkoken/aa-standingsrequests) - Alliance Auth compatible standings tool module for requesting alt character standings and checking API key registration.
- [Erik Kalkoken / aa-structuretimers](https://gitlab.com/ErikKalkoken/aa-structuretimers) - An app for keeping track of Eve Online structure timers with Alliance Auth and Discord
- [Erik Kalkoken / aa-structures](https://gitlab.com/ErikKalkoken/aa-structures) - A plugin for monitoring alliance structures incl. a structure browser and structure notifications on Discord.
- [AaronKable / aa-discordbot](https://github.com/Solar-Helix-Independent-Transport/allianceauth-discordbot) - A Discordbot for AllianceAuth that can interact with the Django project, send Channel and Direct Messages and be extended by other Community Apps.
- [AaronKable / aa-corpstats-two](https://github.com/Solar-Helix-Independent-Transport/allianceauth-corpstats-two) - Extended Corpstats module.
- [AaronKable / aa-statistics](https://github.com/Solar-Helix-Independent-Transport/aa-statistics) - Periodically gather and update statistics, primarily for use by other modules
- [AaronKable / allianceauth-blacklist](https://github.com/Solar-Helix-Independent-Transport/allianceauth-blacklist) - Integration with Alliance Auth's State System, creates an maintains a Blacklisted State to ensure no services access is granted to Blacklisted users
- [AaronKable / allianceauth-corp-tools](https://github.com/Solar-Helix-Independent-Transport/allianceauth-corp-tools) - A fully modular character/corporation audit, built to underpin allianceauth-secure-groups and other apps.
- [AaronKable / allianceauth-corp-tools-buybacks](https://github.com/Solar-Helix-Independent-Transport/allianceauth-ct-buybacks) - Adds a simple page that can show members the market orders that are currently active from an entity as a buyback.
- [AaronKable / allianceauth-corp-tools-indy-dash](https://github.com/Solar-Helix-Independent-Transport/allianceauth-corp-tools-indy-dash) - Adds a simple page that can show members the structures and rigs that are fitted, without giving access to full structure information.
- [AaronKable / allianceauth-corp-tools-moons](https://github.com/Solar-Helix-Independent-Transport/allianceauth-corp-tools-moons) - A Simple moon mining module primarily for taxation of moon mining by using allianceauth-invoice-manager.
- [AaronKable / allianceauth-corp-tools-pinger](https://github.com/Solar-Helix-Independent-Transport/allianceauth-corp-tools-pinger) - An Eve Alert/Notification system that leverages allianceauth-corp-tools data to auto balance pings across an entity.
- [AaronKable / allianceauth-group-assigner](https://github.com/Solar-Helix-Independent-Transport/allianceauth-group-assigner) - Admin app to add groups to users when they change States.
- [AaronKable / allianceauth-invoice-manager](https://github.com/Solar-Helix-Independent-Transport/allianceauth-invoice-manager) - A module for processing incoming payments to a holding for.
- [AaronKable / allianceauth-mumble-tagger](https://github.com/pvyParts/allianceauth-mumble-tagger) - This is a simple plugin to append a "Tag" too the end of a display name on mumble depending on a users group association.
- [AaronKable / allianceauth-secure-groups](https://github.com/Solar-Helix-Independent-Transport/allianceauth-secure-groups) - Groups with required or automatic filters, Supports apps for Skills, Item, Activity or any checks you can think of making.
- [AaronKable / allianceauth-signal-pings](https://github.com/Solar-Helix-Independent-Transport/allianceauth-signal-pings) - A simple plugin to send a "signal" to a discord webhook when a user does something in auth.
- [ppfeufer / aa-esi-status](https://github.com/ppfeufer/aa-esi-status) - App for Alliance Auth to show the current status of ESI and its end points.
- [ppfeufer / aa-fleetpings](https://github.com/ppfeufer/aa-fleetpings) - App that can format your fleet pings and also ping for you to Discord and Slack.
- [ppfeufer / aa-forum](https://github.com/ppfeufer/aa-forum) - Simple integrated forum for Alliance Auth
- [ppfeufer / aa-rss-to-discord](https://github.com/ppfeufer/aa-rss-to-discord) - Alliance Auth module to post news from RSS feeds to your Discord
- [ppfeufer / aa-sov-timer](https://github.com/ppfeufer/aa-sov-timer) - Sovereignty campaign overview for Alliance Auth.
- [ppfeufer / aa-srp](https://github.com/ppfeufer/aa-srp) - Improved SRP module
- [ppfeufer / aa-timezones](https://github.com/ppfeufer/aa-timezones) - Displaying different time zones within Alliance Auth.
- [ppfeufer / allianceauth-afat](https://github.com/ppfeufer/allianceauth-afat) - Another Fleet Activity Tracker
- [Maestro-Zacht / allianceauth-graphql](https://github.com/Maestro-Zacht/allianceauth-graphql) - GraphQL API For Alliance Auth.
- [Maestro-Zacht / allianceauth-pve](https://github.com/Maestro-Zacht/allianceauth-pve) - App for helping groups manage PvE loot and payments.
- [Maestro-Zacht / aa-charlink](https://github.com/Maestro-Zacht/aa-charlink) - A simple app for AllianceAuth that allows users to link each character to all the AllianceAuth apps with only 1 login action.
- [Maestro-Zacht / aa-ravworks-exporter](https://github.com/Maestro-Zacht/aa-ravworks-exporter) - A simple app for AllianceAuth that allows users to export their data into an industry config for Ravworks.
- [Maestro-Zacht / aa-contacts](https://github.com/Maestro-Zacht/aa-contacts) - App for tracking corp and alliance contacts (i.e. standings).
- [Tactical Supremacy / aa-gdpr](https://gitlab.com/tactical-supremacy/aa-gdpr) - A Collection of resources to help Alliance Auth installs meet GDPR legislations.
- [Tactical Supremacy / aa-relays](https://gitlab.com/tactical-supremacy/aa-relays) - For forwarding, collating and filtering of messages from various chat services to defined outputs including Database logging
- [Tactical Supremacy / aa-market-manager](https://gitlab.com/tactical-supremacy/aa-market-manager) - Market Browser and Market Management tools such as Supply Alerts to ensure adequate stock in defined locations.
- [Tactical Supremacy / aa-alumni](https://gitlab.com/tactical-supremacy/aa-alumni) - Automatically manage an Alumni state for past members of an Alliance and/or Corporation.
- [Tactical Supremacy / aa-drifters](https://gitlab.com/tactical-supremacy/aa-drifters) - Drifter wormhole tracker/manager plugin for Alliance Auth.
- [Tactical Supremacy / aa-incursions](https://gitlab.com/tactical-supremacy/aa-incursions) - Incursion Tools and Notifications for Alliance Auth.
- [Tactical Supremacy / aa-secret-santa](https://gitlab.com/tactical-supremacy/aa-secret-santa) - A Secret Santa Manager
- [Eclipse Expeditions / aa-blueprints](https://gitlab.com/eclipse-expeditions/aa-blueprints) - An app that provides a list of corporate blueprints for reference by members.
- [Eclipse Expeditions / aa-inactivity](https://gitlab.com/eclipse-expeditions/aa-inactivity) - An app that allows corps to track member activity as well as leave of absence requests.
- [Ikarus Cesaille / aa-opcalendar](https://gitlab.com/paulipa/allianceauth-opcalendar) - A calendar type view for events and fleets. Supports manual events with custom labels, public NPSI fleet imports and importing events over ESI from the ingame calendars.
- [Ikarus Cesaille / aa-buybackprogram](https://gitlab.com/paulipa/allianceauth-buyback-program) - Allows a character or a corporation to run contract based buyback programs
- [arctiru / aa-miningtaxes](https://gitlab.com/arctiru/aa-miningtaxes) - Allows corps to track and tax all mining activity of it's members.
- [Meowosaurus / aa-simplewiki](https://github.com/meowosaurus/aa-simplewiki) - App to create your own wiki inside alliance auth
- [Meowosaurus / aa-srppayouts](https://github.com/meowosaurus/aa-srppayouts) - Add a SRP payouts page to your auth (reimbursement table)
- [r0kym / metenox](https://gitlab.com/r0kym/aa-metenox) - Allows to manage corporations' metenoxes and appraise how much a metenoxed moon would yield.
- [r0kym / dens](https://gitlab.com/r0kym/aa-dens) - Allows to manage users' mercenary dens and creates timers on timberboard or structuretimers.
- [r0kym / evescout](https://gitlab.com/r0kym/aa-evescout) - Adds new cogs to allianceauth-discordbot related to the [eve-scout](https://eve-scout.com/) API (automated pings for thera/turnur connections, nearby jove observatories, ...).
- [r0kym / evictionsplit](https://gitlab.com/r0kym/aa-evictionsplit) - Allow users to record how long they spend on standby/doorstop during a wh eviction to decide payouts.

## Services

- [Alliance Auth / mumble-authenticator](https://gitlab.com/allianceauth/mumble-authenticator) - Authenticator script for the Alliance Auth Mumble integration
- [AaronKable / allianceauth-mumbletemps](https://github.com/Solar-Helix-Independent-Transport/allianceauth-mumble-temp) - Enables Templink access to Mumble for non authed characters
- [AaronKable / allianceauth-wiki-js](https://github.com/Solar-Helix-Independent-Transport/allianceauth-wiki-js) - Allianceauth service module for Wiki.js
- [Adarnof / allianceauth-mattermost](https://gitlab.com/Adarnof/allianceauth-mattermost) - service module for Mattermost

## Themes

- [ppfeufer / aa-theme-slate](https://github.com/ppfeufer/aa-theme-slate) - Bootstrap theme Slate converted into a theme for Alliance auth.

## Deployment and Administration

- [AaronKable / AllianceAuth-Celery-Analytics](https://github.com/Solar-Helix-Independent-Transport/allianceauth-celeryanalytics) - Celery task output logging to database for easy viewing and monitoring
- [MillerUK / AllianceAuth-Docker](https://github.com/milleruk/alliance_auth_docker) -  Docker-Compose Stack - Including Traefik Proxy
- [MillerUK/ AllianceAuth-Docker-Image](https://hub.docker.com/r/milleruk/allianceauth) - Docker image for AllianceAuth
- [Erik Kalkoken / aa-package-monitor](https://gitlab.com/ErikKalkoken/aa-package-monitor) - An app that helps keep track of installed packages and outstanding updates for Alliance Auth.
- [Erik Kalkoken / aa-taskmonitor](https://gitlab.com/ErikKalkoken/aa-taskmonitor) - An Alliance Auth app for monitoring celery tasks.
- [Tactical Supremacy / aa-top](https://gitlab.com/tactical-supremacy/aa-top) - System Utilization and AA Statistics plugin for Alliance Auth. Inspired by <https://zkillboard.com/ztop/> by Squizz Caphinator

## Development and Infrastructure

- [Erik Kalkoken / allianceauth-app-utils](https://gitlab.com/ErikKalkoken/allianceauth-app-utils) - Utility functions and classes for rapid development of Alliance Auth apps
- [Erik Kalkoken / allianceauth-example-plugin](https://gitlab.com/ErikKalkoken/allianceauth-example-plugin) - Example plugin app (GitLab Version) that can be used as starting point / template for developing new plugin apps
- [ppfeufer / aa-example-plugin](https://github.com/ppfeufer/aa-example-plugin) - Example plugin app (GitHub Version) that can be used as starting point / template for developing new plugin apps
- [Erik Kalkoken / discordproxy](https://gitlab.com/ErikKalkoken/discordproxy) - Proxy server for accessing the Discord API via gRPC
- [Erik Kalkoken / django-eveuniverse](https://gitlab.com/ErikKalkoken/django-eveuniverse) - Complete set of Eve Online Universe models for Django with on-demand loading from ESI
- [Tactical Supremacy / aa-routing](https://gitlab.com/tactical-supremacy/aa-routing) - Routing/Pathfinding library for AA.